#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <vector>
#include <algorithm>
#include <mutex>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace SyncWithMutex
{
    class DataProcessor
    {
        vector<int> data_;
        bool is_ready_ = false;
        mutex mtx_;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(3s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            cout << "End reading..." << endl;

            lock_guard<mutex> lk{mtx_};
            is_ready_ = true;
        }

        void process(int id)
        {
            {
                unique_lock<mutex> lk{mtx_};

                while(!is_ready_)
                {
                    lk.unlock();
                    this_thread::yield();
                    lk.lock();;
                }
            }

            long sum = accumulate(data_.begin(), data_.end(), 0L);
            cout << "Id: " << id << " Sum: " << sum << endl;
        }
    };
}

namespace SyncWithAtomics
{
    class DataProcessor
    {
        vector<int> data_;
        atomic<bool> is_ready_{};
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(3s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            cout << "End reading..." << endl;

            is_ready_ = true;
        }

        void process(int id)
        {
            while(!is_ready_)
            {
            }

            long sum = accumulate(data_.begin(), data_.end(), 0L);
            cout << "Id: " << id << " Sum: " << sum << endl;
        }
    };

}

class DataProcessor
{
    vector<int> data_;
    bool is_ready_ = false;
    condition_variable cv_ready_;
    mutex cv_mtx_;
public:
    void read()
    {
        cout << "Start reading..." << endl;

        this_thread::sleep_for(3s);

        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; });

        cout << "End reading..." << endl;

        unique_lock<mutex> lk{cv_mtx_};
        is_ready_ = true;
        lk.unlock();
        cv_ready_.notify_all();
    }

    void process(int id)
    {
        {
            unique_lock<mutex> lk{cv_mtx_};
            cv_ready_.wait(lk, [this] { return is_ready_;});
        }

        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << "Id: " << id << " Sum: " << sum << endl;
    }
};


int main()
{
    //using namespace SyncWithAtomics;

    DataProcessor dp;

    thread thd1{[&dp] { dp.read(); }};  // watek przygotowujacy dane

    thread thd2{[&dp] { dp.process(1); }};
    thread thd3{[&dp] { dp.process(2); }};

    thd1.join();
    thd2.join();
    thd3.join();
}
