#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void may_throw(int data)
{
    if (data % 13 == 0)
        throw logic_error("Error#13 for " + to_string(data));

    if (data % 17)
        throw int{17};
}

void background_work(int id, int data, exception_ptr& excpt)
{
    this_thread::sleep_for(500ms);

    try
    {
        may_throw(data);
    }
    catch(...)
    {
        excpt = current_exception();
        return;
    }

    this_thread::sleep_for(500ms);
}

int main() try
{
    const int no_of_threads = 20;

    vector<thread> threads(no_of_threads);
    vector<exception_ptr> exceptions_from_threads(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
        threads[i] = thread(background_work, i+1, i, ref(exceptions_from_threads[i]));

    for(auto& thd : threads)
        thd.join();

    // handling of exceptions
    for(auto& ex : exceptions_from_threads)
    {
        try
        {
            if (ex)
            {
                rethrow_exception(ex);
            }
        }
        catch(const logic_error& e)
        {
            cout << "Cought logic_error: " << e.what() << endl;
        }
        catch(const exception& e)
        {
            cout << "Cought generic error: " << e.what() << endl;
        }
    }
}
catch(...)
{
    cout << "Caught exception outside main" << endl;
}
