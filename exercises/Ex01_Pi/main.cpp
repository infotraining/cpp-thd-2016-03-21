#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>

using namespace std;

long calc_hits(long N)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);
    long counter = 0;
    for (int n = 0; n < N; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }
    return counter;
}

void calc_hits_with_threads(long N, long& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);
    long counter = 0;
    for (int n = 0; n < N; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }

    hits += counter;
}

void calc_hits_with_mutex(long N, long& hits, mutex& mtx)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);
    for (int n = 0; n < N; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            lock_guard<mutex> lk{mtx};
            hits++;
        }
    }
}

void calc_hits_with_atomics(long N, atomic<long>& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);
    for (int n = 0; n < N; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            ++hits;
        }
    }
}

void sync_pi(long N)
{
    cout << "Pi calc - synchronous" << endl;

    auto start = chrono::high_resolution_clock::now();
    cout << double(calc_hits(N) / double(N)) * 4 << endl;
    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed sync = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}

void thread_unsafe_pi(long N)
{
    cout << "Pi calc - thread unsafe" << endl;

    long counter = 0;

    auto start = chrono::high_resolution_clock::now();

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    vector<thread> threads(no_of_threads);

    long no_of_throws = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{calc_hits_with_threads, no_of_throws, ref(counter)};
    }

    for(auto& thd : threads)
       thd.join();

    double pi = (static_cast<double>(counter) / N) * 4;

    auto end = chrono::high_resolution_clock::now();

    cout << pi << endl;

    cout << "Elapsed threads 1 = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}


void thread_pi_1(long N)
{
    cout << "Pi calc - threads 1" << endl;

    auto start = chrono::high_resolution_clock::now();

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    vector<thread> threads(no_of_threads);
    vector<long> counters(no_of_threads);

    long no_of_throws = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{calc_hits_with_threads, no_of_throws, ref(counters[i])};
    }

    for(auto& thd : threads)
       thd.join();

    double pi = (accumulate(counters.begin(), counters.end(), 0.0) / N) * 4;

    auto end = chrono::high_resolution_clock::now();

    cout << pi << endl;

    cout << "Elapsed threads 1 = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}

void thread_pi_2(long N)
{
    cout << "Pi calc - threads 2" << endl;

    auto start = chrono::high_resolution_clock::now();

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    vector<thread> threads(no_of_threads);
    vector<long> counters(no_of_threads);

    long no_of_throws = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{[i, &counters, no_of_throws] { counters[i] = calc_hits(no_of_throws);}};
    }

    for(auto& thd : threads)
       thd.join();

    double pi = (accumulate(counters.begin(), counters.end(), 0.0) / N) * 4;

    auto end = chrono::high_resolution_clock::now();

    cout << pi << endl;

    cout << "Elapsed threads 1 = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}

void pi_with_mutex(long N)
{
    cout << "Pi calc - with mutex" << endl;

    long counter = 0;
    mutex mtx;

    auto start = chrono::high_resolution_clock::now();

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    vector<thread> threads(no_of_threads);

    long no_of_throws = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{calc_hits_with_mutex, no_of_throws, ref(counter), ref(mtx)};
    }

    for(auto& thd : threads)
       thd.join();

    double pi = (static_cast<double>(counter) / N) * 4;

    auto end = chrono::high_resolution_clock::now();

    cout << pi << endl;

    cout << "Elapsed threads 1 = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}

void pi_with_atomics(long N)
{
    cout << "Pi calc - with atomics" << endl;

    atomic<long> counter{};
    mutex mtx;

    auto start = chrono::high_resolution_clock::now();

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    vector<thread> threads(no_of_threads);

    long no_of_throws = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{calc_hits_with_atomics, no_of_throws, ref(counter) };
    }

    for(auto& thd : threads)
       thd.join();

    double pi = (static_cast<double>(counter) / N) * 4;

    auto end = chrono::high_resolution_clock::now();

    cout << pi << endl;

    cout << "Elapsed threads 1 = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << " ms" << endl;
}


int main()
{
    long N = 100'000'000;

    sync_pi(N);

    cout << endl;

    thread_unsafe_pi(N);

    cout << endl;

    pi_with_mutex(N);

    cout << endl;

    pi_with_atomics(N);

    cout << endl;

    thread_pi_1(N);

    cout << endl;

    thread_pi_2(N);
}

