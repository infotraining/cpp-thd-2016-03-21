#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

class BankAccount
{
    using Mutex = recursive_mutex;

    const int id_;
    double balance_;
    mutable Mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";
        lock_guard<Mutex> lk{mtx_};
        cout << balance_ << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
//        lock_guard<Mutex> lk_from{mtx_};
//        lock_guard<Mutex> lk_to{to.mtx_};  // causes deadlock!!!

        unique_lock<Mutex> lk_from{mtx_, defer_lock};
        unique_lock<Mutex> lk_to{to.mtx_, defer_lock};

        lock(lk_from, lk_to);  // avoiding deadlock

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        lock_guard<Mutex> lk{mtx_};

        balance_ -= amount;
    }

    void deposit(double amount)
    {
        lock_guard<Mutex> lk{mtx_};

        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        lock_guard<Mutex> lk{mtx_};

        return balance_;
    }
};

void make_deposits(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.deposit(amount);
}

void make_withdraws(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.withdraw(amount);
}

void make_transfer(int counter, BankAccount& from, BankAccount& to, double amount)
{
    for(int i = 0; i < counter; ++i)
    {
       //cout << "transfer #" << i << " from: " << from.id() << " to " << to.id() << endl;
       from.transfer(to, amount);
    }
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    cout << "Before: " << endl;

    ba1.print();
    ba2.print();

    ba1.transfer(ba2, 1.0);
    ba2.transfer(ba1, 1.0);

    long no_of_operations = 100'000;

    thread thd1{make_deposits, no_of_operations, ref(ba1), 1.0};
    thread thd2{make_withdraws, no_of_operations, ref(ba1), 2.0};

    thread thd3{make_transfer, no_of_operations, ref(ba1), ref(ba2), 1.0};
    thread thd4{make_transfer, no_of_operations, ref(ba2), ref(ba1), 1.0};

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();

    cout << "\nAfter: " << endl;

    ba1.print();
    ba2.print();

}
