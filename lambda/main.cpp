#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <algorithm>
#include <cassert>

using namespace std;

void callable_fun(int x)
{
    cout << "callable(" << x << ")" << endl;
}

class CallableFunctor
{
    unsigned int counter_ = 0;
public:
    void operator()(int x)
    {
        ++counter_;
        cout << "CallbaleFunctor.operator(" << x << ")" << endl;
    }

    unsigned int counter() const
    {
        return counter_;
    }
};


template <typename InputIterator, typename Callable>
Callable my_for_each(InputIterator start, InputIterator end, Callable fun)
{
    for(InputIterator it = start; it != end; ++it)
        fun(*it);  // call

    return fun;
}

class MagicLambda_23465237645
{
public:
    void operator()() const
    {
        cout << "lambda" << endl;
    }
};


class MagicLambda_54353453453
{
    int& sum;
    const int factor;
public:
    MagicLambda_54353453453(int& sum, int factor) : sum{sum}, factor{factor}
    {}

    void operator()(int x) const
    {
        sum += x * factor;
    }
};

template <typename T>
auto multiply(T x, T y) -> decltype(x * y)
{
    return x * y;
}

void call_wraper(function<void(int)> f, int arg)
{
    cout << "calling!!!" << endl;
    f(arg);
}

int main()
{
    // 1-st category
    callable_fun(9);

    void (*callable_ptr)(int) = &callable_fun;
    callable_ptr(10);

    call_wraper(callable_fun, 13);

    vector<int> vec = { 1, 2, 3, 4, 5 };

    cout << "foreach: \n";
    my_for_each(vec.begin(), vec.end(), callable_fun);
    cout << "\n";

    // 2-nd category
    CallableFunctor cf;  // instancja klasy
    cf(9);
    cf(10);
    call_wraper(cf, 13);

    cout << "cf called " << cf.counter() << " times" << endl;

    cout << "foreach: \n";
    auto functor = for_each(vec.begin(), vec.end(), CallableFunctor());
    cout << "No of calls: " << functor.counter() << endl;
    cout << "\n";

    // 3-rd category
    auto lambda = []() { cout << "lambda" << endl; };
    lambda();

    void (*callback)() = [] { cout << "my callback" << endl; };
    callback();

    cout << "foreach: \n";
    for_each(vec.begin(), vec.end(), [](int x) { cout << "call from lambda: " << x << endl; });
    cout << "\n";

    int sum = 0;
    int factor = 2;
    function<void(int)> adder = [&sum, factor](int x) { sum += x * factor; };  // auto would be better option
    for_each(vec.begin(), vec.end(), adder);

    cout << "sum: " << sum << endl;

    call_wraper([](int x) { cout << "lambda with arg: " << x << endl;}, 13);

    //typedef vector<int> data;
    using data = vector<int>;

    vector<data> vec_data = { {1, 2, 3}, {4, 5, 6}, {7, 8} };

    auto total = accumulate(vec_data.begin(), vec_data.end(), 0,
                            [](int v, const data& d) { return accumulate(d.begin(), d.end(), v); });

    cout << "total: " << total << endl;


}
