#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <mutex>
#include <atomic>
#include <memory>

using namespace std;

namespace Atomics
{
    class LazySingleton
    {
    public:
        LazySingleton(const LazySingleton&) = delete;
        LazySingleton& operator=(const LazySingleton&) = delete;

        void use()
        {
            lock_guard<mutex> lk {mtx_};
            cout << "using Singleton(address: " << this << ")" << endl;
        }

        ~LazySingleton()
        {
            cout << "~Singleton()" << endl;
        }

        static LazySingleton& instance()
        {
            // double check locking pattern
            if (!instance_)
            {
                lock_guard<mutex> lk{mtx_};

                if (!instance_)
                {
                    LazySingleton* temp = new LazySingleton();

                    instance_ = temp;
                }
            }

            return *instance_;
        }

    private:
        static atomic<LazySingleton*> instance_;
        static std::mutex mtx_;

        LazySingleton()
        {
            cout << "Singleton()" << endl;
        }
    };

    std::mutex LazySingleton::mtx_;
    atomic<LazySingleton*> LazySingleton::instance_ {};
}

namespace MagicStatics
{
    class LazySingleton
    {
    public:
        LazySingleton(const LazySingleton&) = delete;
        LazySingleton& operator=(const LazySingleton&) = delete;

        void use()
        {
            lock_guard<mutex> lk {mtx_};
            cout << "using Singleton(address: " << this << ")" << endl;
        }

        ~LazySingleton()
        {
            cout << "~Singleton()" << endl;
        }

        static LazySingleton& instance()
        {
            static LazySingleton unique_instance;
            return unique_instance;
        }

    private:
        static std::mutex mtx_;
        LazySingleton()
        {
            cout << "Singleton()" << endl;
        }
    };

    std::mutex LazySingleton::mtx_;
}



// Proxy pattern
class Subject
{
public:
    virtual void run() = 0;
    virtual ~Subject() = default;
};

class RealSubject : public Subject
{
    vector<int> data_;
public:
    RealSubject() : data_{1'000'000}
    {
        cout << "RealSubject()" << endl;
    }

    void run() override
    {
        cout << "RealSubject.run()" << endl;
    }
};

class Proxy : public Subject
{
    unique_ptr<RealSubject> real_;
    once_flag init_flag_;
public:
    Proxy()
    {
        cout << "Proxy()" << endl;
    }

    void run()
    {
        call_once(init_flag_, [this] { real_ = make_unique<RealSubject>(); });

        real_->run();
    }
};

void client(Subject& s)
{
    s.run();
}

int main()
{
    cout << "Start main" << endl;

    using Singleton = MagicStatics::LazySingleton;

    thread thd1{[] { Singleton::instance().use(); }};

    Singleton& s = Singleton::instance();
    s.use();

    thd1.join();

    auto proxy = make_unique<Proxy>();

    cout << "Spawning thread..." << endl;

    thread thd2{[&proxy] { client(*proxy); }};

    proxy->run();

    thd2.join();
}
