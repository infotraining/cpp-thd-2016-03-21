#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <memory>

using namespace std;

class Gadget
{
    int id_;
    string name_;
public:
    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    Gadget(const Gadget& g) : id_{g.id_}, name_{g.name_}
    {
        cout << "cc Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    Gadget(Gadget&& g) noexcept : id_{g.id_}, name_{move(g.name_)}
    {
        cout << "mv Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    ~Gadget()
    {
        cout << "cleanup Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    void use()
    {
        cout << "using Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }
};

void use_gadget(Gadget* g)
{
    if (g)
        g->use();
}

void use_gadget(Gadget& g)
{
    g.use();
}

void use_and_destroy_gadget(unique_ptr<Gadget> g)
{
    if (g)
        g->use();
}

unique_ptr<Gadget> use_and_pass_gadget(unique_ptr<Gadget> g)
{
    if (g)
        g->use();

    return g;
}

int main()
{
    {
        Gadget* ptr_g = new Gadget(1, "ipad");

        use_gadget(ptr_g);
        ptr_g->use();

        delete ptr_g;
    }

    cout << "\n\n";

    {
        unique_ptr<Gadget> up_cpp11{ new Gadget{1, "ipad"}};

        unique_ptr<Gadget> up_cpp14 = make_unique<Gadget>(2, "ipod");

        up_cpp14->use();

        unique_ptr<Gadget> up2 = move(up_cpp11);  // transfer of ownership
        up2->use();
        assert(up_cpp11 == nullptr);

        use_gadget(up2.get());
        use_gadget(*up2);

        use_and_destroy_gadget(move(up2));
        use_and_destroy_gadget(make_unique<Gadget>(5, "mp3 player"));

        use_and_pass_gadget(move(up_cpp14));
    }

    cout << "\n\n";

    vector<Gadget> gg;

    gg.push_back(Gadget{1, "i1"});
    gg.push_back(Gadget{2, "i1"});
    gg.push_back(Gadget{3, "i1"});
    gg.push_back(Gadget{4, "i1"});
    gg.push_back(Gadget{5, "i1"});

}
