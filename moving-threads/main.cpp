#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>

using namespace std;

class BackgroundWork
{
    const int id_;
public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(int delay)
    {
      for(int i = 0; i < 20; ++i)
      {
          cout << "BW#" << id_ << ": " << i << endl;
          this_thread::sleep_for(chrono::milliseconds(delay));
      }

      cout << "BW#" << id_  << " is finished..." << endl;
    }
};

thread create_thread(int delay)
{
    static int id_gen = 0;

    thread thd{BackgroundWork{++id_gen}, delay};
    return thd;
}

int main()
{
    cout << "no of cores: " << max(thread::hardware_concurrency(), 1u) << endl;

    thread thd1{BackgroundWork{1}, 200};

    thread thd2 = move(thd1);

    assert(thd1.get_id() == thread::id{}); //  thd1 is NotAThread

    thd2.join();

    cout << "\n\nthread-group:\n";

    auto thd3 = create_thread(100);
    auto thd4 = create_thread(400);

    vector<thread> thread_group;

    thread_group.push_back(thread{BackgroundWork{3}, 500});
    thread_group.push_back(create_thread(300));

    thread_group.push_back(move(thd3));
    thread_group.push_back(move(thd4));

    thread_group.emplace_back(BackgroundWork{5}, 200);

    for(auto& thd : thread_group)
        thd.join();
}
