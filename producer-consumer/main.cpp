#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <string>
#include <boost/algorithm/string.hpp>

#include "thread_safe_queue.hpp"

using namespace std;

using DataType = string;

const DataType end_of_work{};

class ProducerConsumer
{
    ThreadSafeQueue<DataType> q_;
public:
    virtual ~ProducerConsumer() = default;

    void produce(const DataType& data, unsigned int consumer_count)
    {
        DataType item = data;

        while(next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            q_.push(item);
            this_thread::sleep_for(50ms);
        }

        for(unsigned int i = 0; i < consumer_count; ++i)
            q_.push(end_of_work);
    }

    void consume(int id)
    {
        while (true)
        {
            DataType item;
            q_.wait_and_pop(item);  // waiting for data

            if (item == end_of_work)
            {
                cout << "end of job - consumer#" << id << endl;
                return;
            }

            cout << "Consumer#" << id << " is processing an item: " << process_data(item) << endl;
        }
    }
protected:
    virtual DataType process_data(const DataType& data) const
    {
        this_thread::sleep_for(300ms);
        return boost::to_upper_copy(data);
    }
};

int main()
{
    ProducerConsumer pc;

    thread thd1{[&pc] { pc.produce("abcde", 2); }};
    thread thd2{[&pc] { pc.consume(1); }};
    thread thd3{[&pc] { pc.consume(2); }};

    thd1.join();
    thd2.join();
    thd3.join();
}
