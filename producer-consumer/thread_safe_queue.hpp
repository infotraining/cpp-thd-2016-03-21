#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <condition_variable>
#include <queue>
#include <mutex>

template <typename T>
class ThreadSafeQueue
{
public:
    ThreadSafeQueue() = default;

    ThreadSafeQueue(const ThreadSafeQueue&) = delete;
    ThreadSafeQueue& operator=(const ThreadSafeQueue&) = delete;

    bool empty() const
    {
        std::lock_guard<std::mutex> lk{mtx_};
        return q_.empty();
    }

    void push(const T& item)
    {
        std::lock_guard<std::mutex> lk{mtx_};
        q_.push(item);
        cv_item_pushed_.notify_one();
    }

    void push(std::initializer_list<T> items)
    {
        std::lock_guard<std::mutex> lk{mtx_};
        for(const auto& item : items)
            q_.push(item);

        cv_item_pushed_.notify_all();
    }

    bool try_pop(T& item)
    {
        std::lock_guard<std::mutex> lk{mtx_};

        if (!q_.empty())
        {
            item = q_.front();
            q_.pop();
            return true;
        }

        return false;
    }

    void wait_and_pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};

        cv_item_pushed_.wait(lk, [this]{ return !q_.empty();});
        item = q_.front();
        q_.pop();
    }

private:
    std::queue<T> q_;
    mutable std::mutex mtx_;
    std::condition_variable cv_item_pushed_;
};

#endif // THREAD_SAFE_QUEUE_HPP
