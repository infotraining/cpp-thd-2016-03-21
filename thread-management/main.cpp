#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void hello(const string& text)
{
    for(const auto& c : text)
    {
        cout << c << " ";
        this_thread::sleep_for(100ms);
        cout.flush();
    }

    cout << endl;
}

class BackgroundWork
{
    const int id_;
public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(int delay)
    {
      for(int i = 0; i < 10; ++i)
      {
          cout << "BW#" << id_ << ": " << i << endl;
          this_thread::sleep_for(chrono::milliseconds(delay));
      }

      cout << "BW#" << id_  << " is finished..." << endl;
    }
};

void load_data(const string& file_name, vector<int>& vec)
{
    for(const int& item : file_name)
        vec.push_back(item);
}

int main()
{    
    thread not_a_thread;
    cout << "thread's id: " << not_a_thread.get_id() << endl;

    thread thd1{hello, "Hello threads"};
    thread thd2{hello, "Concurrent text"};

    thd1.join();
    thd2.join();

    {
        BackgroundWork bw1(1);
        thread thd3{bw1, 100};
        thread thd4{BackgroundWork{2}, 200};

        thd3.join();
        thd4.join();
    }

    vector<int> data1, data2, data3;

    thread thd5{load_data, "file1.txt", ref(data1)};
    thread thd6{load_data, "different_file.txt", ref(data2)};
    thread thd7{[&data3] {
            thread local_thd{hello, "txt"};
            load_data("file_from_lambda.txt", data3);
            local_thd.detach();
    }};


    thd5.join();
    thd6.join();
    thd7.join();

    cout << "data1: ";
    for(const auto& item : data1)
        cout << item << " ";
    cout << endl;

    cout << "data2: ";
    for(const auto& item : data2)
        cout << item << " ";
    cout << endl;

    cout << "data3: ";
    for(const auto& item : data3)
        cout << item << " ";
    cout << endl;
}
