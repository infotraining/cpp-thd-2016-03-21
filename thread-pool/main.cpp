#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

#include "thread_safe_queue.hpp"

using namespace std;

using Task = function<void()>;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads)
    {
        assert(no_of_threads >= 1);
        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            task_q_.push(end_of_work);

        for(auto& thd : threads_)
            thd.join();
    }

    template <typename Callable>
    void submit(Callable&& t)
    {
        task_q_.push(std::forward<Callable>(t));
    }
private:
    ThreadSafeQueue<Task> task_q_;
    std::vector<std::thread> threads_;
    const static nullptr_t end_of_work;

    void run()
    {
        while(true)
        {
            Task task;

            task_q_.wait_and_pop(task);

            if (task == end_of_work)
                return;

            task(); // calling task in a pool thread
        }
    }
};


void background_task(int id)
{
    cout << "BT#" << id << " starts..." << endl;
    this_thread::sleep_for(chrono::milliseconds(rand() % 3000));
    cout << "BT#" << id << " ends..." << endl;
}

int main()
{
    ThreadPool thread_pool(8);

    thread_pool.submit([] { background_task(1);});
    thread_pool.submit([] { background_task(2);});

    for(int i = 3; i <= 30; ++i)
        thread_pool.submit([i]{ background_task(i);});
}
