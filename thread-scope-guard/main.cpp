#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void long_running_task(int id, int delay)
{
    cout << "start THD#" << id << endl;
    this_thread::sleep_for(chrono::milliseconds(delay));
    cout << "end of THD#" << id << endl;
}

void may_throw()
{
    throw std::logic_error("Error#13");
}

class ThreadJoinGuard
{
    thread& thd_;
public:
    explicit ThreadJoinGuard(thread& thd) : thd_{thd}
    {}

    ThreadJoinGuard(const ThreadJoinGuard&) = delete;
    ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

    ~ThreadJoinGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

class JoinableThread
{
    thread thd_;
public:
    explicit JoinableThread(thread&& thd) : thd_{move(thd)}
    {}

    JoinableThread(const JoinableThread&) = delete;
    JoinableThread& operator=(const JoinableThread&) = delete;

    JoinableThread(JoinableThread&&) = default;
    JoinableThread& operator=(JoinableThread&&) = default;

    thread& get()
    {
        return thd_;
    }

    ~JoinableThread()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

void fun_with_threads()
{
    thread thd1{long_running_task, 1, 500};
    ThreadJoinGuard join_guard1{thd1};
    JoinableThread thd2{thread{long_running_task, 2, 3000}};
    // ThreadJoinGuard copy_g2 = join_guard2; // error - ThreadJoinGuard is nocopyable

    JoinableThread thd3 = move(thd2);

    may_throw();
}

int main()
{
    try
    {
        fun_with_threads();
    }
    catch(const exception& e)
    {
        cout << "Caught: " << e.what() << endl;
    }
}
