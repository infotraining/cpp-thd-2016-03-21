#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <mutex>

using namespace std;

timed_mutex mtx;

void background_worker(int id, int timeout)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    unique_lock<timed_mutex> lk{mtx, try_to_lock};

    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD#" << id << " doesn't own a lock... ";
            cout << "Tries to acquire mutex..." << endl;
        } while (!lk.try_lock_for(chrono::milliseconds(timeout)));
    }

    cout << "Start of job in THD#" << id << endl;

    this_thread::sleep_for(5s);

    lk.unlock();

    this_thread::sleep_for(5s);

    cout << "End of thread THD#" << id << endl;
}

int main()
{
    thread thd1{background_worker, 1, 500};
    thread thd2{background_worker, 2, 1000};

    thd1.join();
    thd2.join();

}
